.global _main ;INICIO
				
_main:
	
        MOV	#1,	W5	; 0 +1 y 1 -1
        BSET	TRISA,	#4 ; IN pata 2	
	;como son 7 segmentos se establecen los pines de salida
	BCLR	TRISB,	#15 ;Segmanto A pata 26
	BCLR	TRISB,	#14 ;Segmento B pata 25 
	BCLR	TRISB,	#13 ;Segmento C pata 24
	BCLR	TRISB,	#11 ;Segmento D pata 22
	BCLR	TRISB,	#9 ;Segmento E pata 18
	BCLR	TRISB,	#8 ;Segmento F pata 17
	BCLR	TRISB,	#7 ;Segmento G pata 16

	MOV	#0, W0	; Se pone el registro W0 en cero	
        MOV	W0, PORTB	;Todos los valores del PORTB SE PONEN EN CERO
	MOV	W0, PORTA	;Pasa lo mismo con PORT A
        

	

	
_NCERO:
;Numero 0 
	;Se encienden todos menos el SEGMNTO G
	MOV	#0,	W1 ;;;El registro 1 funcionara como contador para saber en que numero va la cuenta
	BSET	LATB,	#15
	BSET	LATB,	#14
	BSET	LATB,	#13
	BSET	LATB,	#11
	BSET	LATB,	#9
	BSET	LATB,	#8
	CALL	DELAY1S
NBCERO:	
	BCLR	LATB,	#15
	BCLR	LATB,	#14
	BCLR	LATB,	#13
	BCLR	LATB,	#11
	BCLR	LATB,	#9
	BCLR	LATB,	#8
	CALL	DELAY1S
	DEC	W5,W9
	BRA	Z,_NUNO
	BRA	NZ,_NNUEVE	
_NUNO:        
       ; NUMERO 1
       ;Se encenderian unicamente los segmentos B Y C
        MOV	#1,	W1
	BSET	LATB,	#13; Se asigna un 1
	BSET	LATB,	#14
	CALL	DELAY1S
NBUNO:	
	BCLR	LATB,	#13 ;Despues de 1 seg hay un clear
	BCLR	LATB,	#14
	CALL	DELAY1S
	DEC	W5,W9
	BRA	Z,_NDOS
	BRA	NZ,_NCERO
_NDOS:        
        ; Numero 2
	; Se encienden los segmentos A, B,G,E,y D
	MOV	#2,	W1
    	BSET	LATB,	#15
	BSET	LATB,	#14
	BSET	LATB,	#7
	BSET	LATB,	#9
	BSET	LATB,	#11
	CALL	DELAY1S    ; Se mantienen encendidos durante 2 segundos
NBDOS:	
	BCLR	LATB,	#15
	BCLR	LATB,	#14
	BCLR	LATB,	#7
	BCLR	LATB,	#9
	BCLR	LATB,	#11 ; se resetean
	CALL	DELAY1S   
	DEC	W5,W9
	BRA	Z,_NTRES
	BRA	NZ,_NUNO
_NTRES:      
       ; Numero 3
       ; Se encienden los segmentos A,B,G,C,y D
        MOV	#3,	W1
	BSET	LATB,	#15
	BSET	LATB,	#14
	BSET	LATB,	#7
	BSET	LATB,	#13
	BSET	LATB,	#11
	CALL	DELAY1S      ; Se prende durante 3 segundos
NBTRES:		
	BCLR	LATB,	#15
	BCLR	LATB,	#14
	BCLR	LATB,	#7
	BCLR	LATB,	#13
	BCLR	LATB,	#11 ; se resetea lo que hay en port B
	CALL	DELAY1S
	DEC	W5,W9
	BRA	Z,_NCUATRO
	BRA	NZ,_NDOS
_NCUATRO:     
        ; Numero 4
	;Se encienden los segmentos F,G,B yC
	MOV	#4,	W1
	BSET	LATB,	#8
	BSET	LATB,	#7
	BSET	LATB,	#14
	BSET	LATB,	#13
	CALL	DELAY1S  ; delay de 4 segundos porque es el numero 4
NBCUATRO:	    
        BCLR	LATB,	#8
	BCLR	LATB,	#7
	BCLR	LATB,	#14
	BCLR	LATB,	#13 ; se resetean los valores 
	CALL	DELAY1S
	DEC	W5,W9
	BRA	Z,_NCINCO
	BRA	NZ,_NTRES
_NCINCO:
        ; Numero 5
	;Se encienden A, F,G,C yD
        MOV	#5,	W1
	BSET	LATB,	#15
	BSET	LATB,	#8
	BSET	LATB,	#7
	BSET	LATB,	#13
	BSET	LATB,	#11
	CALL	DELAY1S ; 5 delays porque corresponde al num 5
NBCINCO:	
        BCLR	LATB,	#15
	BCLR	LATB,	#8
	BCLR	LATB,	#7
	BCLR	LATB,	#13 ; se ponen en ceros estos segmentos
	BCLR	LATB,	#11
	CALL	DELAY1S
	DEC	W5,W9
	BRA	Z,_NSEIS
	BRA	NZ,_NCUATRO
_NSEIS:
	; Numero 6 
	; Se enciende A, C,D,E,F y G
        MOV	#6,	W1
	BSET	LATB,	#15
	BSET	LATB,	#13
	BSET	LATB,	#11
	BSET	LATB,	#9
	BSET	LATB,	#8
	BSET	LATB,	#7
	CALL	DELAY1S
NBSEIS:	
	BCLR	LATB,	#15
	BCLR	LATB,	#13
	BCLR	LATB,	#11
	BCLR	LATB,	#9
	BCLR	LATB,	#8
	BCLR	LATB,	#7
	CALL	DELAY1S
	DEC	W5,W9
	BRA	Z,_NSIETE
	BRA	NZ,_NCINCO	
_NSIETE:
	; Numero 7
	;Se enciende A, B y C
        MOV	#7,	W1
	BSET	LATB,	#15
	BSET	LATB,	#14
	BSET	LATB,	#13
	CALL	DELAY1S
NBSIETE:	
	BCLR	LATB,	#15
	BCLR	LATB,	#14
	BCLR	LATB,	#13
	CALL	DELAY1S
	DEC	W5,W9
	BRA	Z,_NOCHO
	BRA	NZ,_NSEIS
_NOCHO:
       ; Numero 8	
       ; Se encienden todos
        MOV	#8,	W1
	BSET	LATB,	#15
	BSET	LATB,	#14
	BSET	LATB,	#13
	BSET	LATB,	#11
	BSET	LATB,	#9
	BSET	LATB,	#8
	BSET	LATB,	#7
	CALL	DELAY1S
NBOCHO:	
	BCLR	LATB,	#15
	BCLR	LATB,	#14
	BCLR	LATB,	#13
	BCLR	LATB,	#11
	BCLR	LATB,	#9
	BCLR	LATB,	#8
	BCLR	LATB,	#7
	CALL	DELAY1S
	DEC	W5,W9
	BRA	Z,_NNUEVE
	BRA	NZ,_NSIETE
_NNUEVE:
	; Numero 9
    ;Se encienden todos los menos los SEGMENTOS D y E
        MOV	#9,	W1
	BSET	LATB,	#15
	BSET	LATB,	#14
	BSET	LATB,	#8
	BSET	LATB,	#7
	BSET	LATB,	#13
	CALL	DELAY1S
NBNUEVE:	
	BCLR	LATB,	#15
	BCLR	LATB,	#14
	BCLR	LATB,	#8
	BCLR	LATB,	#7
	BCLR	LATB,	#13
	CALL	DELAY1S
	DEC	W5,W9
	BRA	Z,_NCERO
	BRA	NZ,_NOCHO
DELAY1S:
	MOV	#1000,	W0	;Moviendo el numero 1000 a W0
	GOTO	REPETIR

DIRECCION:
	COM	W5,W6
	MOV	W6,W5
	GOTO	REPETIR

REPETIR:
	DEC	W0,	W4	
	MOV	W4, 	W0	
	BRA	Z,	ANTIRREBOTE
	GOTO	PULSADOR		
	
PULSADOR:
    	MOV	PORTA,	W2
	AND	#16,W2
	BRA	Z,DIRECCION	
	GOTO	REPETIR

ANTIRREBOTE:

	MOV	#0,W7
	MOV	#0,W8
	SUB	W1,W7,W8
	BRA	Z,NBCERO	

	MOV	#1,W7
	MOV	#0,W8
	SUB	W1,W7,W8
	BRA	Z,NBUNO

	MOV	#2,W7
	MOV	#0,W8
	SUB	W1,W7,W8
	BRA	Z,NBDOS	

	MOV	#3,W7
	MOV	#0,W8
	SUB	W1,W7,W8
	BRA	Z,NBTRES	

	MOV	#4,W7
	MOV	#0,W8
	SUB	W1,W7,W8
	BRA	Z,NBCUATRO

	MOV	#5,W7
	MOV	#0,W8
	SUB	W1,W7,W8
	BRA	Z,NBCINCO

	MOV	#6,W7
	MOV	#0,W8
	SUB	W1,W7,W8
	BRA	Z,NBSEIS

	MOV	#7,W7
	MOV	#0,W8
	SUB	W1,W7,W8
	BRA	Z,NBSIETE

	MOV	#8,W7
	MOV	#0,W8
	SUB	W1,W7,W8
	BRA	Z,NBOCHO

	MOV	#9,W7
	MOV	#0,W8
	SUB	W1,W7,W8
	BRA	Z,NBNUEVE

.END				

